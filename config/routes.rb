Rails.application.routes.draw do
  resources :laps
  resources :pilots

  post '/upload_result', to: 'laps#upload_result'
  root 'laps#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
