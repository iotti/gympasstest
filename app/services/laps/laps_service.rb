class Laps::LapsService
    attr_accessor :melhor_tempo_id, :melhor_tempo

    def initialize
        self.melhor_tempo_id = 0
        self.melhor_tempo = 9999999999
    end

    def fetch
        return [] if Lap.count==0
        results = Lap.all.map.with_index do |lap,index|
            data = {pilot: lap.pilot}
            data[:lap_count] = lap.lap_number
            data[:best_lap_value] = lap.lap_time
            data[:best_lap] = lap.lap_time_string
            data[:total_time] = lap.lap_occurrence
            data[:avg_speed] = lap.avg_speed
            data[:lap_total] = lap.total_time
            data
        end 
        mark_best_lap(results)
        results
    end

protected
    def test_best_lap(lap, index)
        if lap[:best_lap_value] < self.melhor_tempo
            self.melhor_tempo_id = index
            self.melhor_tempo = lap[:best_lap_value]
        end
    end

    def mark_best_lap(results)
        results.each_with_index do |result,index|
            test_best_lap(result,index)
        end
        results[self.melhor_tempo_id][:best_lap] = results[self.melhor_tempo_id][:best_lap] + ' &#9734'
    end


end