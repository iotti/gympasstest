class Laps::RaceResultService < Laps::LapsService
    def fetch
        return [] if Lap.count==0
        results = Pilot.all.map do |pilot|
            build_data_from_pilot(pilot)
        end.sort_by{ |x| x[:lap_total_value]}
        mark_best_lap(results)
        include_distance_to_first(results)
    end

protected
    def include_distance_to_first(results)
        results.each do |r|
            distance_time = Lap.time_formatted(r[:lap_total_value] - results.first[:lap_total_value])
            r[:lap_total] = r[:lap_total] + ' +' + distance_time
        end
    end

    def build_data_from_pilot(pilot)
        lap = {pilot: pilot}
        lap[:lap_count] = pilot.laps.count
        best_lap = pilot.laps.order(:lap_time).first
        lap[:best_lap] = best_lap.lap_time_string
        lap[:best_lap_value] = best_lap.lap_time
        lap[:lap_total_value] = pilot.laps.sum(&:lap_time)
        lap[:avg_speed] = pilot.laps.average(:avg_speed)
        lap[:lap_total] = Lap.time_formatted(lap[:lap_total_value])
        
        lap
    end
end