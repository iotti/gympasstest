class LeitorLog
    attr_accessor :file_path

    OCCURENCE_COL       = 0
    PILOT_NUMBER_COL    = 1
    PILOT_NAME_COL      = 3
    LAP_NUMBER_COL      = 4
    LAP_TIME_COL        = 5
    AVG_SPEED_COL       = 6

    def initialize(file_path)
        self.file_path = file_path
    end

    def execute
        data = read_file
        data.each do |line_data|
            process_data(line_data)
        end
    end

private

    def read_file
        return File.readlines(self.file_path).map do |line|
            read_line(line)
        end.compact
    end

    def read_line(line)
        line_data = line.gsub!(/\s+/, '|').split('|')
        return nil if line_data[0].nil? || line_data[0].upcase == 'HORA'
        line_data
    end

    def process_data(line_data)
        process_pilot(line_data)
        process_lap(line_data)
    end

    def process_pilot(line_data)
        pilot_number = line_data[PILOT_NUMBER_COL]
        pilot_name = line_data[PILOT_NAME_COL]
        pilot = Pilot.where(number: pilot_number).first
        if(pilot.nil?)
            pilot = Pilot.create(number: pilot_number, name: pilot_name)
        end
        pilot
    end

    def process_lap(line_data)
        pilot = process_pilot(line_data)
        lap = Lap.new
        lap.pilot_id = pilot.id
        lap.lap_occurrence = Time.parse(line_data[OCCURENCE_COL])
        lap.lap_number = line_data[LAP_NUMBER_COL].to_i
        lap_time = line_data[LAP_TIME_COL].split(':')
        lap_seconds = lap_time[1].split('.')
        lap.lap_time = (lap_time[0].to_i*60000) + (lap_seconds[0].to_i* 1000) + lap_seconds[1].to_i
        lap.avg_speed = line_data[AVG_SPEED_COL].gsub(',','.').to_f
        lap.save!
    end

end