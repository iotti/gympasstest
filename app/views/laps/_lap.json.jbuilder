json.extract! lap, :id, :pilot_id, :lap_occurrence, :lap_number, :lap_time, :avg_speed, :created_at, :updated_at
json.url lap_url(lap, format: :json)
