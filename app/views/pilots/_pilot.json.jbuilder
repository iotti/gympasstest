json.extract! pilot, :id, :number, :name, :created_at, :updated_at
json.url pilot_url(pilot, format: :json)
