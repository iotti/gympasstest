class Lap < ApplicationRecord
  belongs_to :pilot

  def lap_time_string
    Lap.time_formatted(self.lap_time)
  end

  def total_time
    result = Lap.where(pilot_id: self.pilot_id).sum(&:lap_time)
    Lap.time_formatted(result)
  end

  def self.time_formatted(time)
    milliseconds = time % (60000) 
    seconds = milliseconds / 1000
    milliseconds = milliseconds % 1000
    minutes = time / 60000
    minutes.to_s + ':' + seconds.to_s.rjust(2,'0') + '.' + milliseconds.to_s.rjust(3,'0')
  end
end
