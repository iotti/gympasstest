class CreatePilots < ActiveRecord::Migration[5.2]
  def change
    create_table :pilots do |t|
      t.string :number
      t.string :name

      t.timestamps
    end
    add_index :pilots, :number, unique: true
  end
end
