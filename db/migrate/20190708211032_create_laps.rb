class CreateLaps < ActiveRecord::Migration[5.2]
  def change
    create_table :laps do |t|
      t.references :pilot, foreign_key: true
      t.time :lap_occurrence
      t.integer :lap_number
      t.integer :lap_time
      t.decimal :avg_speed

      t.timestamps
    end
  end
end
