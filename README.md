# Teste Gympass

Este foi um teste feito pro Thiago Iotti para Gympass, iniciado as 16:45 do dia 08/07/2019

* Ruby version: ~> 2.6.0

* Rails version: ~> 5.2.3

* Instruções

    - Este projeto foi feito em Ruby on Rails, pois foi priorizado o entendimento do código.
    - Para instalação do Ruby on Rails: 
        
        > Windows e Mac: http://railsinstaller.org/pt-BR 

        > Linux (Debian): http://railsapps.github.io/installrubyonrails-ubuntu.html

    - Após Ruby On Rails instalado e o projeto clonado, os seguintes comandos devem ser executados no terminal, dentro do diretório do projeto.
        
        > bundle install

        > rails db:create db:migrate db:seed

    - Se ambos os comandos foram executados com êxito, execute o seguinte comando para iniciar o servidor:
        
        > rails s

    - Com isto, um webserver deve ser aberto na prota 3000, para acessá-lo, basta abrir o seu navegador (Chrome ou Firefox) no endereço 'http://localhost:3000'
     